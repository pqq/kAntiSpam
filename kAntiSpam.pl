#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename: kAntiSpam.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com/kAntiSpam)
# started:  10.08.2003
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
#
# version:  v05_20050723
#           - Re-written the bounced email address algorithm to handle all emails from
#             MAILER-DAEMON (that's not internal folder data) and from local user
#             as bounced emails. Selects out the first email address in the body of 
#             these emails, since it seems like this always is the bounced email.
# version:  v05_20050722
#           - Updated bounced email algorithm to handle more types of bounced emails.
#             New algorithm supports bounced emails from hotmail, aol, qmail servers etc.
# version:  v05_20050628
#           - Changed the filelock on mail folders to be exclusive; flock(2).
#             Earlier it was flock(1), or shared reading. This might have
#             caused loss of emails in earlier versions?
#           - Another thing that might cause loss of emails is if the program
#             runs to often (once a minute might be too often). That might 
#             prevent some emails to be written to spool file cause it's 
#             busy / locked by kAntiSpam. Might prevent spooling?
#           - Added subject in log on approved emails.
# version:  v05_20050524
#           - Created an additional check for email validations. Found that
#             the "Mail::RFC822::Address" check wasn't strict enough.
# version:  v05_20050509
#           - Implemented "Mail::RFC822::Address: regexp-based address validation"
#             for email validation
#             http://www.ex-parrot.com/~pdw/Mail-RFC822-Address.html
# version:  v05_20050411
#           - A mistake in last verion that cause loss of multiple senders while forwarding
#             is fixed again.
# version:  v05_20050410
#           - Renamed lockfile to from "kAntiSpam.lock" to "lockfile_kAntiSpam.lock" 
#             (to avoid deleting 'kAntiSpam' by mistake using rm command).
#           - Fixed problem with from-field when forwarding emails.
#           - Changed way of testing start of new email to avoid that illegal 
#             character crashes the program.
#           - Added the possibility to have NOTIFICATION_EMAIL_NAME and EMAIL_ADDRESS 
#             (to be substituted) in the NOTIFICATION_TEXT.
#           - Changed signature text in notification emails.
# version:  v05_20041228
#           - By a mistake the fix that prevented kAntiSpam to crash due to '.' on a 
#             single line was removed. This is now fixed again.
#           - Using Reply-To address in From field, and deleting Reply-To field. This
#             leads to that From field will be used (and From contains all Cc addresses)
#           - Using name of sender, in stead of email address when forwarding messages.
#             So name will now be on the form: "Senders Name (via kAntiSpam)"
# version:  v05_20041222
#           - Forwarding algorithm updated again...
#           - Previously the body was scanned for header fields. This sometimes 
#             led to wrong fields, if body contained "To:" or "Cc:" in the text.
# version:  v05_20041220
#           - Forwarding error causing "broken pipe" (Sendmail). Fixed the way the
#             from field was built to avoid ',,'. This most likely caused the problem.
# version:  v05_20041212
#           - Updated forwarding algorithm (again).
# version:  v05_20041209
#           - Fixed forwarding algorithm to keep most of the existing email header
#             fields. This should now preserve all information about multipart 
#             emails.
# version:  v05_20041208
#           - Re-written the forwarding algorithm to support all attachments and
#             to keep the information about Cc and multiple To addresses. This is 
#             done by adding the multiple addresses to the 'From' field.
# version:  v05_20041129
#           - Other Mime-Multipart attachment bugs discovered and fixed.
# version:  v05_20041127
#           - Attachment forwarding error fixed.
# version:  v05_20041126
#           - Fixed a bug in the email forwarding procedure that would cause
#             an infinite loop.
# version:  v05_20041124
#           - Fixed a bug that caused '.' on a single line to end the program 
#             abnormally in the email forwarding routine.
#           - Updated the program so emails with approved word, in the process of
#             being approved, will be forward as well.
#           - Updated the forwarding algorithm to support all email types, not
#             just plain text emails.
#           - Fixed a bug that caused clean email addresses to be logged in
#             "notified.log"
# version:  v05_20041115
#           - Added functionality to log notification emails sent.
#           - Feature to set a maximum number of notification emails to be sent.
#           - Functionality added to make it possible to specify what group of
#             senders (SPAM or DELETED) that will receive notification emails.
#           - Notification email text changed.
#           - Functionality added for forwarding of clean emails.
#           - Changed the licence check so it's now possible to use kAntiSpam
#             without a licence. However the email subject will be changed.
# version:  v04_20040830
#           - Created lock/unlock algorithms to prevent multiple programs running
#             at the same time. This prevents abnormal behaviour due to errors in
#             the program.
#           - New check of email address when parsing inbox. If email address
#             contains '*' it is set to invalid (spam).
# version:  v04_20040825
#           - Updated email address check when parsing inbox, put a 200 character
#             limit on addresses. Program crashed when email addresses where 280 
#             characters long (sometimes this length from fake address).
# version:  v04_20040806
#           - updated the rex.exp of email address before sending mail
# version:  v04_20040727
#           - updated the rex.exp of email address before sending mail
# version:  v04_20040710
#           - Loading all values from "config.ini" at once, at start of program
#           - Changed the regular expression for finding the first line in a email,
#             the previous one had an bug when receiving messages sent from same mail
#             server as the kAntiSpam runned on.
#           - Changed the reg.exp for finding email address in mail bounced from hotmail 
#             account
# version:  v04_20040708
#           - Checking approved sender before bounced email
#           - Write out the approved word in debugging message when approving a sender
# version:  v04_20040705
#           - Added an simple email-format check before sending a notification email
# version:  v04_20040703
#           - Fixed a bug when "DELETE_IMMEDIATELY" option was chosen.
#           - Changed so all "MAILER-DAEMON" messages will be treated as SPAM (and not be deleted)
# version:  v04_20040702
#           - Changed the notification algorithm so it won't notify the owner of the
#             email account. This happened when spam mails had account owners email as sender.
# version:  v04_20040701
#           - Bounced email algorithm now supports bounced messages from qmail-server
#           - Option to set senders name of notification emails in "config.ini"
#           - Changed the format of the notification email
# version:  v04_20040630
#           - Added option to turn debug information on/off in "config.ini"
#           - Added option for "full-debug" or "simple-debug"
#           - Emails from yourself will not be added to approved list when containing approved word.
#             Reason for this is that you might email yourself. If you had the approved word
#             in your signature this happened before. And yeah, those spam bastards will sometimes 
#             use your email address as sender.
#           - Automatic notification of blocked senders via email (using sendmail)
#           - Added functionality to automatically delete emails from senders with fake accounts
#             At the moment this only tested for "hotmail" accounts and other using the same 
#             format on their error handling. 
# version:  v03_20040522
#           - Checking body, additional to subject, for "automatic approve word"
# version:  v03_20040410
#           - Email with no subject will be treated as banned email if sender is un-approved
#           - Email body is checked against a banned wordlist
#           - Support for regular expressions in banned word lists
#           - Support for regular expressions in approved list (email addresses)
# version:  v03_20040329
#           - Added the option to either delete emails immediately, or store deleted 
#             mails in a deleted file/mailbox.
#           - Automatically finds the path to the directory where kAntiSpam is running,
#             so no hardcoding of paths to output and config file is needed
# version:  v03_20040320
#           - Added support for licence control
#           - Moved all initial/config values to a seperate file
#           - If any emails contains a special word/string in it's subject it will be 
#             imported automatically, and it's sender will be added to the approved list
#           - Added functionality to import banned subject words the same way as 
#             import approved senders (via saving emails in a special banned folder)
#           - Added support for "banned subject words", emails from un-approved
#             senders containing any banned words will be deleted and not stored
#             in the spam folder
#           - "MAILER-DAEMON" messages in inbox that is not "INTERNAL-DATA"
#             messages are treated as spam messages
#           - Re-wrote big parts of the program to make it more scalable
# version:  v02_20040202
#           - Fixed a bug concerning the keeping of the "MAILER-DAEMON" messages
# version:  v02_20040201
#           - Changed the algorithms so messages from "MAILER-DAEMON" won't be
#             deleted/removed from different mailboxes/files.
#           - Added import mechanism, so that all senders of emails stored in a
#             special "IMPORT" folder will be automatically added to the 
#             approved list. Then these emails will be moved to the inbox.
#           - Added readable timestamp in debugging output
#           - Removed the procedures to load spamfile and clean-inbox, since 
#             it is far better to just append the messages
# version:  v01_20030813
#           - Filtering out lines starting with '#' in approvedList.txt, so it's
#             possible to add comments in that list. Re-written "loadApprovedSenders"
#             sub procedure to do filtering here in stead of in "parseInbox"
# version:  v01_20030812
#           - Added a timstamp to the debugline where sender is checked
# version:  v01_20030810
#           - The first version of this program was made.
# ------------------------------------------------------------------------------------





# -------------------
# use
# -------------------
use strict;
use Address qw(valid validlist);





# -------------------
# declarations
# -------------------
my $version					= "v05_20050723";														# the version number of this program, used in debugging
my $magic					= "180876";																# the magic number, used to check the licence
my $path 					= &getPath;																# retrieve the path to where this program is running
my $logFile					= $path . "output.log";													# the file where all the output is written to
my $iniFile					= $path . "config.ini";													# the file containing all configuration values
my $lockFile				= $path . "lockfile_kAntiSpam.lock";									# lockfile
my $notifiedFile			= $path . "notified.log";												# log file when sending notification emails

my $unregisteredSubject		= "Unregistered version of kAntiSpam!";									# subject will be changed to this kAntiSpam isn't licenced

my $development				= "no";																# if "yes" development mode, set to "no" before releasing a version

# ********************************************************************************************************************
# load all values from "config.ini", this is done all at once to ease development in case values has to be changed
# ********************************************************************************************************************
my $automaticApproveWord	= &getIniValue("AUTOMATIC_APPROVE_WORD");								# every email containing this word/string will be imported and it's sender will be approved

my $inbox					= &getIniValue("INBOX");												# the mail inbox
my $cleanInbox				= &getIniValue("CLEAN_INBOX");											# the clean inbox
my $spamFile				= &getIniValue("SPAM_FILE");											# the spam mailbox
my $importFile 				= &getIniValue("IMPORT_FILE");											# the import mailbox
my $bannedsubjectwordsFile	= &getIniValue("BANNEDSUBJECTWORDS_FILE");								# the banned subject words mailbox
my $deletedFile				= &getIniValue("DELETED_FILE");											# the file/inbox where the deleted emails will be stored, or if this value is DELETE_IMMEDIATLY they won't be stored at all

my $approvedList			= &getIniValue("APPROVED_LIST");										# the approved senders
my $bannedSubjectWordsList	= &getIniValue("BANNEDSUBJECTWORDS_LIST");								# the banned subject words
my $bannedBodyWordsList		= &getIniValue("BANNEDBODYWORDS_LIST");									# the banned body words

my $notifyBlockedSenders 	= &getIniValue("NOTIFY_BLOCKED_SENDERS");								# notify blocked senders by sending them an email?
my $sendmailPath			= &getIniValue("SENDMAIL_PATH");										# path to sendmail program
my $notificationText		= &getIniValue("NOTIFICATION_TEXT");									# users own defined email text
my $notificationEmailName	= &getIniValue("NOTIFICATION_EMAIL_NAME");								# the name of the sender of the automatic generated emails
my $maxNotificationsSent	= &getIniValue("MAX_NOTIFICATIONS_SENT");								# the maximum number of notification emails to send
my $notifySpam				= &getIniValue("NOTIFY_SPAM");											# if this is true senders of emails treated as SPAM will receive automatic email notification
my $notifyDeleted			= &getIniValue("NOTIFY_DELETED");										# if this is true senders of emails treated as DELETED will receive automatic email notification

my $forwardEmails			= &getIniValue("FORWARD_EMAILS");										# forward clean emails?
my $forwardAddress			= &getIniValue("FORWARD_ADDRESS");										# email address to forward all clean emails to

my $licenceKey				= &getIniValue("LICENCE_KEY");											# the licence

my $debug					= &getIniValue("DEBUG");												# if $debug==1 show/print simple debug info, if 2 print full, else (==0) hide debug info
# ********************************************************************************************************************

# ----- VALUES USED FOR DEVELOPMENT MODE ONLY -----
if ($development eq "yes"){
	$inbox 					= "C:/Documents and Settings/klevstul/mail/inbox";
	$cleanInbox 			= "C:/Documents and Settings/klevstul/mail/clean";
	$spamFile 				= "C:/Documents and Settings/klevstul/mail/spam";
	$importFile 			= "C:/Documents and Settings/klevstul/mail/import";
	$bannedsubjectwordsFile = "C:/Documents and Settings/klevstul/mail/bannedsubjectwords";
	$deletedFile			= "C:/Documents and Settings/klevstul/mail/deleted";
	$approvedList 			= "C:/klevstul/private/projects/kAntiSpam/approvedList.txt";
	$bannedSubjectWordsList = "C:/klevstul/private/projects/kAntiSpam/bannedSubjectWords.txt";
	$bannedBodyWordsList 	= "C:/klevstul/private/projects/kAntiSpam/bannedBodyWords.txt";
	$notifyBlockedSenders 	= "no";
	$forwardEmails			= "yes";
	$debug 					= "2;"
}
# ----- / END OF LINES USED FOR TESTING / -----

my @approvedSenders;																				# an array containing the approved senders loaded from file
my @bannedSubjectWords;																				# an array containing all banned subject words
my @bannedBodyWords;																				# an array containing all banned body words

my $cleanInboxContent;																				# the clean spam-free inbox content is stored here
my $spamFileContent;																				# the spam content is put here
my $importContent;																					# the content of the messages in the import file
my $deletedContent;																					# the emails that are deleted are stored here

my $systemMessageInbox;																				# the internal system message for the inbox folder
my $systemMessageImport;																			# the internal system message for the import folder
my $systemMessageBannedsubjectwords;																# the internal system message for the banned subject words folder

my $emailCounter;																					# the total number of emails read from the mailbox
my %fullEmail;																						# a hash for storing the full emails when parsing them
my %subject;																						# a hash for storing all the emails subjects
my %body;																							# a hash for storing all message bodies
my %from;																							# a hash for storing all the emails senders
my %deliveredTo;																					# a hash for storing "Delivered-To" address (eventually only one address)

my $bouncedEmailAddresses;																			# emails that bounce back because of wrong email address

my %notificationLogEntries;																			# a hash containing addresses and number of times an notification email has been sent





# -------------------
# main
# -------------------
&lock;																								#    Write lockfile, a new kAntiSpam program can't be runned before this file is removed (unlocked)
&debug("kAntiSpam.pl version $version started", 2);

&parseMailfolder($bannedsubjectwordsFile);															# 1: Check if there are any emails in the 'banned subject words' folder.
&processBannedsubjectwordsEmails;																	#    If there are, save the subject in the 'banned subject word' file, and delete the email.

&parseMailfolder($importFile);																		# 2: Check if there are any emails in the import folder.
&processImportEmails;																				#    If there are, save the sender of the emails in the approved list, and move the email to the inbox.

&loadApprovedSenders;																				# 3: Load all the approved senders,
&loadBannedSubjectWords;																			#    the 'banned subject words'
&loadBannedBodyWords;																				#    and the 'banned body words'.
&loadNotificationLogfile;																			#    Load all info about earlier notifications that are sent

&parseMailfolder($inbox);																			# 4: Go through all emails in the original inbox.
&processInboxEmails;																				# case 1: MAILER-DAEMON: store the internal data system message, only this will be kept when emptying the mail folder
																									# case 2: handle bounced emails
																									# case 3: approved sender: mail goes directly to clean inbox
																									# case 4: un-approved sender

&cleanUp($spamFile);																				# 5: - Clean spam folder (delete emails from bounced addresses)
&cleanUp($deletedFile);																				#    - Clean deleted folder (if there is a deleted folder)
&updateNotificationLogfile;																			#    - Updates the entries in the notification logfile


if ($development ne "yes"){
&writeCleanInbox;																					# 5: Add new messages to the clean inbox,
&writeSpamfile;																						#    add new spam emails to the spam folder,
&writeInbox;																						#    empty the original inbox,
&writeImport;																						#    empty the import folder/mailbox,
&writeBannedsubjectwords;																			#    empty the 'banned subject words' folder/mailbox
&writeDeleted;																						#    add deleted emails to the deleted folder
}

&unlock;																							#    Delete lockfile
&debug("kAntiSpam.pl finished", 2);





# -------------------
# sub
# -------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Opens and parses a mail folder/file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub parseMailfolder{
	my $folder = $_[0];																				# path to file (folder) to parse
	my $line;
	my $isBody;

	&resetVariablesForEmailImport;																	# resets the variables the email will be stored in

	# -------------------------------------------------------------------
	# flock values:
	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	# sub LOCK_SH()  { 1 }     #  Shared lock (for reading)
	# sub LOCK_EX()  { 2 }     #  Exclusive lock (for writing)
	# sub LOCK_NB()  { 4 }     #  Non-blocking request (don't stall)
	# sub LOCK_UN()  { 8 }     #  Free the lock (careful!)
	# -------------------------------------------------------------------

	open (FILE, "<$folder") || &error("Could not open '$folder'");
	flock (FILE, 2);																				# exclusive lock, don't allow anyone to access file (system wil have to wait before spooling to the file)
	# -------------------------------------------
	# go through all lines of the inbox file
	# -------------------------------------------
	while ($line = <FILE>){
		# ---
		# Formats of line that starts a new email (the "From" line), and the subject line:
		# ---
		# From frode.klevstul@student.unsw.edu.au Sat Aug 02 10:08:18 2003
		# From frode  Thu Jul  8 16:45:22 2004
		# Subject: This is the subject of the email
		# -/-
		#                     sender  day    month   date  timestamp year

		if ($line =~ m/^From\s(.+)\s+(\w*)\s(\w*)\s+(\d+)\s(\S+)\s(\d{4})/){						# all new emails starts with this pattern
			$emailCounter++;
			$from{$emailCounter} = $1;

			if ($from{$emailCounter} ne "MAILER-DAEMON"){											# only perform these tests if it's not a system email
				if ( length($from{$emailCounter}) > 200 ){											# program doesn't handle email addresses longer than 200 characters, so check this to prevent crash
					$from{$emailCounter} = "INVALID_ADDRESS-TOO_LONG";								# replace the too long address with a string that won't go through the email sending procedure's check (so no notification will be sent here)
				} elsif ( !&validEmail($from{$emailCounter}) ){										# check if the sender's address has the right format
					$from{$emailCounter} = "INVALID_ADDRESS-WRONG_FORMAT";							# replace the too long address with a string that won't go through the email sending procedure's check (so no notification will be sent here)
				}
			}
			
			$fullEmail{$emailCounter} .= $line;
			$isBody = 0;
		} elsif ($line =~ m/^Delivered-To:\s(.*)$/ && $deliveredTo{$emailCounter} eq ""){			# NB: This field does not exists in all emails, so it's not bulletproof to use this to check the licence...
			$deliveredTo{$emailCounter} .= $1;
		} elsif ($line =~ m/^Subject:\s(.*)$/ && $subject{$emailCounter} eq ""){
			$subject{$emailCounter} = $1;
			$fullEmail{$emailCounter} .= $line;
		} else {
			$fullEmail{$emailCounter} .= $line;
			
			if ($isBody == 0 && $line =~ m/^$/s ) {													# if it's the first blank line after a new email has started...
				$isBody = 1;																		# ...we know that the body will start after this line
			} elsif ($isBody == 1){
				$body{$emailCounter} .= $line;
			}
		}
	}
	close (FILE);
	flock (FILE, 8);																				# unlock file
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# process emails containing banned subject words (import banned subject words)
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub processBannedsubjectwordsEmails{
	my $i;
	my $noEmailsImported;

	open (FILE, ">>$bannedSubjectWordsList") || &error("Could not open '$bannedSubjectWordsList'");
	flock (FILE, 2);																				# share file for writing

	if ( $emailCounter > 1) {																		# if we have something to import, write out a timestamp for when it is imported
		$noEmailsImported = $emailCounter - 1;
		print FILE "# Imported $noEmailsImported banned subject word(s) at " . &timestamp . ":\n";
	}

	for ($i=1; $i<=$emailCounter; $i++){

		if ( $from{$i} eq "MAILER-DAEMON" && $subject{$i} eq "DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA"){
			$systemMessageBannedsubjectwords .= $fullEmail{$i};
		} else {
			print FILE $subject{$i} . "\n";
			&debug("'$subject{$i}' added to banned subject words", 1);
		}
	}

	close (FILE);
	flock (FILE, 8);																				# unlock file	
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# process emails from the import folder (import approved senders)
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub processImportEmails{
	my $i;
	my $noEmailsImported;

	open (FILE, ">>$approvedList") || &error("Could not open '$approvedList'");
	flock (FILE, 2);																				# share file for writing

	if ( $emailCounter > 1) {																		# if we have something to import, write out a timestamp for when it is imported
		$noEmailsImported = $emailCounter - 1;
		print FILE "# Imported $noEmailsImported sender(s) at " . &timestamp . ":\n";
	}

	for ($i=1; $i<=$emailCounter; $i++){

		if ( $from{$i} eq "MAILER-DAEMON" && $subject{$i} eq "DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA"){
			$systemMessageImport .= $fullEmail{$i};
		} else {
			$importContent .= $fullEmail{$i};
			print FILE $from{$i} . "\n";
			&debug("'$from{$i}' => ADDED as an approved sender", 1);
		}
	}

	close (FILE);
	flock (FILE, 8);																				# unlock file	
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# process the emails from the inbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub processInboxEmails{
	my $i;
	my $registeredUser;

	for ($i=1; $i<=$emailCounter; $i++){
		&debug("Processing email from '$from{$i}'".
		       #"\n                   with subject '$subject{$i}'".
		       "...", 2);

		# licence control
		if ($deliveredTo{$i} ne ""){																# mails from CRON-DAEMON, MAILER-DAEMON etc won't have a "Delivered-To" value
			$registeredUser = &verfiyLicenceKey($deliveredTo{$i});									# check if the licence is valid for the email address the email is delivered to
		} else {																					# we don't want to do anything to system mails etc., so we set this to registered
			$registeredUser = 1;
		}

		# case 1: MAILER-DAEMON: store the internal data system message, only this will be kept when emptying the mail folder
		if ( $from{$i} eq "MAILER-DAEMON" && $subject{$i} eq "DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA"){
			$systemMessageInbox .= $fullEmail{$i};

		# case 2: approved sender: mail goes directly to clean inbox
		} elsif ( &approvedSender($from{$i}) == 1 ) {
			if ($registeredUser){																	# check if it is a registered user
				$cleanInboxContent .= $fullEmail{$i};
			} else {																				# if unregisted user we change the subject
				$fullEmail{$i}	=~ s/$subject{$i}/$unregisteredSubject/s;						
				$cleanInboxContent .= $fullEmail{$i};
				$subject{$i}		= $unregisteredSubject;
			}
			&debug("'$from{$i}' => APPROVED - subject: '$subject{$i}'", 1);
			&forwardEmail($deliveredTo{$i}, $from{$i}, $subject{$i}, $fullEmail{$i});				# this will forward the clean email to a forward address if this address is set in the config file
		# case 3: Check if the email has bounced (from field is then "MAILER-DAEMON" or "INVALID_ADDRESS-WRONG_FORMAT"), if it is a bounced email: process it...
		} elsif ($from{$i} !~ '@') {
			my $bouncedEmail = &getBouncedEmailaddress($body{$i});
			$bouncedEmailAddresses .= "���" . $bouncedEmail;										# store the email address that had an error
			&debug("Bounced email address: '$bouncedEmail'", 2);

		# case 4: un-approved sender
		} elsif ($from{$i} =~ '@') {																# emails on wrong format and from mailer daemon won't contain an @ (INVALID_ADDRESS-WRONG_FORMAT, MAILER-DAEMON)

			# case 4-1: subject or body contains the "automatic approve word": import sender and move mail to clean inbox
			if ( $subject{$i} =~ m/$automaticApproveWord/i || $body{$i} =~ m/$automaticApproveWord/i ){
				$importContent .= $fullEmail{$i};

				if ($from{$i} ne $deliveredTo{$i} && $from{$i} ne "MAILER-DAEMON" ){				# your own email address (emails from yourself) will not be added to the approved list, and not "MAILER-DAEMON" (in case error messages contains the approved word, which happens when email bounces)

					open (FILE, ">>$approvedList") || &error("Could not open '$approvedList'");
					flock (FILE, 2);																# share file for writing
					print FILE "# Imported an 'automaticApproveWord sender': at " . &timestamp . ":\n";
					print FILE $from{$i} . "\n";
					close (FILE);
					flock (FILE, 8);																# unlock file	

					&debug("'$from{$i}' => APPROVED: added ('$automaticApproveWord' in email)", 1);
					&forwardEmail($deliveredTo{$i}, $from{$i}, $subject{$i}, $fullEmail{$i});		# this will forward the clean email to a forward address if this address is set in the config file
				}

			# case 4-2: subject contains banned word: store in delted folder (or if "DELETE_IMMEDIATLY" don't store it at all)
			} elsif ( (&bannedSubject($subject{$i}) == 1) && $from{$i} ne "MAILER-DAEMON" ){		# we won't delete MAILER-DAEMON mails
				$deletedContent .= $fullEmail{$i};
				&debug("'$from{$i}' => DELETED - subject: '$subject{$i}'", 1);
				if ($notifyDeleted eq "yes"){
					&sendNotification($deliveredTo{$i}, $from{$i}, $subject{$i}, $body{$i});
				}

			# case 4-3: empty subject -> treated as banned emails
			} elsif ($subject{$i} eq "") {															# error messages from mailer daemon won't have empty subjects
				$deletedContent .= $fullEmail{$i};
				&debug("'$from{$i}' => DELETED - no subject", 1);
				if ($notifyDeleted eq "yes"){
					&sendNotification($deliveredTo{$i}, $from{$i}, $subject{$i}, $body{$i});
				}

			# case 4-4: body contains banned word -> treated as banned subject emails
			} elsif ( (&bannedBody($body{$i}) == 1) && $from{$i} ne "MAILER-DAEMON"  ){				# don't want to check mails from MAILER-DAEMON, want them to be treated as SPAM
				$deletedContent .= $fullEmail{$i};
				&debug("'$from{$i}' => DELETED - subject: '$subject{$i}'", 1);
				if ($notifyDeleted eq "yes"){
					&sendNotification($deliveredTo{$i}, $from{$i}, $subject{$i}, $body{$i});
				}

			# case 4-5: store email in spam folder
			} else {
				$spamFileContent .= $fullEmail{$i};
				&debug("'$from{$i}' => SPAM - subject: '$subject{$i}'", 1);
				if ($notifySpam eq "yes"){
					&sendNotification($deliveredTo{$i}, $from{$i}, $subject{$i}, $body{$i});
				}
			}
		# case 5: left-over emails (should be impossible to come here)
		} else {
			$deletedContent .= $fullEmail{$i};
		}
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load approved senders
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub loadApprovedSenders{
	my $line;

	open (FILE, "<$approvedList") || &error("Could not open '$approvedList'");
	flock (FILE, 1);																				# share reading, don't allow writing
	while (chop($line = <FILE>)){
		if ($line eq "" || $line =~ m/^\s*#/){
			next;
		} else {
			push(@approvedSenders, $line);
		}
	}
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load banned subject words
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub loadBannedSubjectWords{
	my $line;

	open (FILE, "<$bannedSubjectWordsList") || &error("Could not open '$bannedSubjectWordsList'");
	flock (FILE, 1);																				# share reading, don't allow writing
	while (chop($line = <FILE>)){
		if ($line eq "" || $line =~ m/^\s*#/){
			next;
		} else {
			push(@bannedSubjectWords, $line);
		}
	}
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load banned body words
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub loadBannedBodyWords{
	my $line;

	open (FILE, "<$bannedBodyWordsList") || &error("Could not open '$bannedBodyWordsList'");
	flock (FILE, 1);																				# share reading, don't allow writing
	while (chop($line = <FILE>)){
		if ($line eq "" || $line =~ m/^\s*#/){
			next;
		} else {
			push(@bannedBodyWords, $line);
		}
	}
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# load notification logfile
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub loadNotificationLogfile{
	my $line;
	my $address;
	my $noNotifications;

    open(FILE, "<$notifiedFile") || &error("failed to open '$notifiedFile'");
	flock (FILE, 1);
	while ($line = <FILE>){
		$line =~ s/\n//g;
		($address, $noNotifications) = split('\t', $line);
		$notificationLogEntries{$address} = $noNotifications;
	}
	close (FILE);
	flock (FILE, 8);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# check if sender is approved (sender = email address)
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub approvedSender{
	my $emailAddress = $_[0];
	my $approved;

	foreach $approved (@approvedSenders){															# go through all approved senders
		if ($emailAddress ne "" && $emailAddress =~ m/$approved/i){									# compare the sender with all approved
			return 1;																				# if we have a match, it's approved
		}
	}
	return 0;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# check if subject contains any banned words
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub bannedSubject{
	my $subject = $_[0];
	my $word;

	foreach $word (@bannedSubjectWords){
		if ($subject ne "" && $subject =~ m/$word/i){
			&debug("Banned word '$word' in subject of email:\n", 2);
			return 1;
		}
	}
	return 0;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# check if subject contains any banned words
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub bannedBody{
	my $body = $_[0];
	my $word;

	foreach $word (@bannedBodyWords){
		if ($body ne "" && $body =~ m/$word/i){
			&debug("Banned word '$word' in body of email:\n", 2);
			return 1;
		}
	}
	return 0;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# write (append) to the clean inbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeCleanInbox{
	open (FILE, ">>$cleanInbox") || &error("Could not open '$cleanInbox'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $cleanInboxContent;
	print FILE $importContent;																		# we add all the messages from the import file
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# write (append) to the spam mailbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeSpamfile{
	open (FILE, ">>$spamFile") || &error("Could not open $spamFile'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $spamFileContent;
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# empty the original inbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeInbox{
	open (FILE, ">$inbox") || &error("Could not open '$inbox'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $systemMessageInbox;
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# empty the import mailbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeImport{
	open (FILE, ">$importFile") || &error("Could not open '$importFile'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $systemMessageImport;
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# empty the banned subject words mailbox
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeBannedsubjectwords{
	open (FILE, ">$bannedsubjectwordsFile") || &error("Could not open '$bannedsubjectwordsFile'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $systemMessageBannedsubjectwords;
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# write the deleted emails
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeDeleted{
	if ($deletedFile !~ m/DELETE_IMMEDIATELY/){														# don't write them to file if user wants to delete them immediatly	
		open (FILE, ">>$deletedFile") || &error("Could not open '$deletedFile'");
		flock (FILE, 2);																			# lock file for writing
		print FILE $deletedContent;
		close (FILE);
		flock (FILE, 8);																			# unlock file
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# resets the values before emails are read into different variables
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub resetVariablesForEmailImport{
	my $i;
	
	for ($i=1; $i<=$emailCounter; $i++){
		$fullEmail{$i}		= "";
		$subject{$i}		= "";
		$body{$i}			= "";
		$from{$i}			= "";
		$deliveredTo{$i}	= "";
	}
	$emailCounter = 0;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints out a debug message if debug is activated
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub debug{
	my $txt				= $_[0];
	my $minDebugLevel = $_[1];

	if ($debug >= $minDebugLevel){
		$txt .= "\n";
		$txt =~ s/\n\n/\n/;
		&printIt("(".&timestamp."): $txt");
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints out an error message and dies / exits
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub error{
	my $txt	= $_[0];
	$txt .= "\n";
	$txt =~ s/\n\n/\n/;
	&printIt("ERROR: $txt");
	die("ERROR: $txt");
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# print to log file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub printIt{
	my $txt	= $_[0];
	open (FILE, ">>$logFile") || die("Could not open '$logFile'");
	flock (FILE, 2);																				# lock file for writing
	print FILE $txt;
	close (FILE);
	flock (FILE, 8);																				# unlock file

	if ($development eq "yes"){
		print $txt;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# timestamp, formats the time
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub timestamp{
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;
	
	$year = $year+1900;
	$mon = $mon+1;

	# puts a zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	# 19991109_182455
	$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;	

	return $timestamp;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load initial config values
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getIniValue{
	my $parameter = $_[0];																			# the name of the parameter we want it's value to
	my $line;
	my $value;

    open(FILE, "<$iniFile") || &error("failed to open '$iniFile'");
	WHILE: while ($line = <FILE>){
		if ($line =~ m/^#/){
			next WHILE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			$value = $2;
			last WHILE;
		}	
	}
	close (FILE);

	if ($value eq ""){
		&error("Failed to find a value for parameter '$parameter' in '$iniFile'");
	}

	return $value;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# check if the licence is valid
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub verfiyLicenceKey{
	my $deliveredTo = $_[0];

	my $emailValue = &getEmailValue($deliveredTo);
	my ($number1,$number2,$number3,$number4) = split('-',$licenceKey);
	my $result = $number1 - $number2 + $number3 - $number4 + $emailValue;

	if ($result != $magic){																			# invalid licence
		&debug("Invalid licence for email address '$deliveredTo'",2);
		return 0;
	} else {																						# valid licence
		return 1;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# calculates a number based on a email address
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub getEmailValue{
	my $email = $_[0];
	my ($part1,$part2) = split('\@',$email);
	my ($part3,$part4) = split('\.',$email);
	my $sum = ((length($part1) * length($part3) + length($part2) * length($part3)) * length($part4)) * length($part3);
	my $sum2 = 0;
	my @emailCharacter = split(//,$email);
	foreach (@emailCharacter){
		$sum2 += ord($_);
	}
	my $totalSum = $sum + $sum2;
	return $totalSum;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# find the path, where kAntiSpam is running
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub getPath{
	$0=~/^(.+[\\\/])[^\\\/]+[\\\/]*$/;
	my $cgidir= $1 || "./";
	return $cgidir;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# notify senders that their email is blocked, using sendmail
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub sendNotification{
	my $deliveredTo	= $_[0];
	my $from 		= $_[1];
	my $subject		= $_[2];
	my $body		= $_[3];



	if ( 
		($notifyBlockedSenders eq "yes") && 														# check that we want to notify blocked senders
		($deliveredTo ne $from) &&																	# don't send notification email to yourself (if spam mails uses your email address)
		( &validEmail($from)) &&																	# check that the email address is on the right format
		($notificationLogEntries{$from} < $maxNotificationsSent)									# check that we haven't reached the limit of number of notifications sent to this address
		){			
		

		my $signature 				= "- - - - FORWARD THIS EXACT EMAIL TO - - - -\n";
		   $signature              .= "          '$deliveredTo'\n";
		   $signature              .= "- - - - - KEEPING THESE LINES - - - - - - -\n";
		   $signature              .= "          '$automaticApproveWord'\n";
		   $signature              .= "- - - - - - - - - - - - - - - - - - - - - -\n";
		   $signature              .= "\n";
		   $signature              .= "Forwarding this exact email will ensure safe delivery of this and future\n";
		   $signature              .= "emails to $notificationEmailName ($deliveredTo). ";
		   $signature              .= "\n";
		   $signature              .= "\n";
		   $signature              .= "This email was automatically generated by kAntiSpam $version.\n";
		   $signature              .= "For more information about kAntiSpam go to this site:\n";
		   $signature              .= "---\n";
		   $signature              .= "http://klevstul.com/kAntiSpam\n";
		   $signature              .= "---\n";
		   $signature              .= "kAntiSpam is a trademark of klevstul.com";

		$notificationText	=~ s/\[NOTIFICATION_EMAIL_NAME\]/$notificationEmailName/sg;
		$notificationText	=~ s/\[EMAIL_ADDRESS\]/$deliveredTo/sg;
		$notificationText	=~ s/\\n/\n/g;															# have to do this substitution to get newline in the mail
	
		&debug("Sending notification email to '$from' about '$subject'", 2);	
	
		open (MAIL, "|$sendmailPath -t") || &error("starting sendmail '$sendmailPath': $!");
		print MAIL "From: $notificationEmailName (kAntiSpam generated) <$deliveredTo>\n";
		print MAIL "Reply-to: $deliveredTo\n";
		print MAIL "To: $from\n";
		print MAIL "Subject: Please resend/forward this exact email to '$deliveredTo' [$automaticApproveWord]!\n";
		print MAIL "Content-type: text/plain\n\n";
		print MAIL "$notificationText\n\n\n";
		print MAIL "$signature\n\n\n";
		print MAIL "----- Original message below: -----\n";
		print MAIL "Subject: $subject\n\n";
		print MAIL "$body";
		print MAIL "-----  /End of org. message   -----\n";
		close(MAIL) || &error("closing mail: $!");
	
		&debug("Mail successfully sent", 2);
		
		$notificationLogEntries{$from}++;															# increase the number of notification emails sent by 1
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# forward email
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub forwardEmail{
	my $deliveredTo 	= $_[0];
	my $fromAddress		= $_[1];
	my $subject			= $_[2];
	my $fullEmail		= $_[3];

	my @fullEmailArray	= split('\n', $fullEmail);

	my $mimeVersion;
	my $contentType;
	my $boundary;
	my $contentTransferEncoding;
	my $to;
	my $cc;
	my $from;
	my $fromName;
	my $replyTo;
	my $newFrom;
	my $nextLine		= undef;
	my $nextLineDiff	= undef;
	my $nextLineTo		= undef;
	my $nextLineCc		= undef;
	my $nextLineFrom	= undef;
	my $line;
	my $i				= 0;
	my $isBody			= undef;

	if ( 
		$forwardEmails eq "yes" &&
		&validEmail($forwardAddress)																# check that the email address is on the right format
		){			
		&debug("Forwarding email to '$forwardAddress' about '$subject'", 2);	

		FOREACH: foreach $line (@fullEmailArray){

			if ($line eq "" || $isBody==1){															# if we have an empty line it's the end of the email header, the only thing to do then is to check...
				$isBody = 1;
				$line =~ s/^\.$/. /;																# ...if '.' is written on it's own line the mail program ends/thinks it's the last line of the email, so we have to add a space
				next FOREACH;																		# the stuff below is only ment for the header, so we can jump to next line
			}

			if ($line =~ m/,\s?$/){																	# if the line ends with an comma the next line is a part of the same field / type
				$nextLine = "yes";
			} elsif ($fullEmailArray[$i+1] ne "" && $fullEmailArray[$i+1] =~ m/^\s+/){				# if next line starts with space it's also a part of the current field
				$nextLine = "yes";
			} else {
				$nextLine = "";
			}

			if ($line =~ m/^Return-Path:\s(.*)$/ || $line =~ m/^Received:\s(.*)$/ || $nextLineDiff ne ""){
				$fullEmailArray[$i] = "[EMPTY_LINE]";												# delete the line
				
				if ($nextLine ne ""){																# if the line ends with a comma the next line is to be interpreted as part of the same field
					$nextLineDiff = "yes";															# set a new next line variable to true
				} else {																			# this is the last (or only) line of the field
					$nextLineDiff = "";
				}
			} elsif ($line =~ m/^DomainKey-Signature:\s(.*)$/ || $nextLineDiff ne "" && $line =~ m/(.*)/){
				$fullEmailArray[$i] = "[EMPTY_LINE]";
				
				if ($nextLine ne ""){
					$nextLineDiff = "yes";
				} else {
					$nextLineDiff = "";
				}
			} elsif ($line =~ m/^To:\s(.*)$/ || $nextLineTo ne "" && $line =~ m/(.*)/){
				$to .= $1;																			# store the "To:" line
				$fullEmailArray[$i] = "[EMPTY_LINE]";												# delete the line
				
				if ($nextLine ne ""){																# if the line ends with a comma the next line is to be interpreted as part of the same field
					$nextLineTo = "yes";
				} else {																			# this is the last (or only) line of the field
					$fullEmailArray[$i] = "To: $forwardAddress";
					$nextLineTo = "";
				}
			} elsif ($line =~ m/^Cc:\s(.*)$/i || $nextLineCc ne "" && $line =~ m/(.*)/){
				$cc .= $1;																			# store the "Cc:" line
				$fullEmailArray[$i] = "[EMPTY_LINE]";												# delete the line
				
				if ($nextLine ne ""){																# if the line ends with a comma the next line is to be interpreted as part of the same field
					$nextLineCc = "yes";
				} else {																			# this is the last (or only) line of the field
					$nextLineCc = "";
				}
			} elsif ($line =~ m/^From:\s(.*)$/ || $nextLineFrom ne "" && $line =~ m/(.*)/){
				$from		.= $1;																	# store the "From:" line
				$fromName	= $from;
				$fromName	=~ s/^(.*)[<](.*)[>]$/$1/;
				
				$fullEmailArray[$i] = "[EMPTY_LINE]";												# delete the line
				
				if ($nextLine ne ""){																# if the line ends with a comma the next line is to be interpreted as part of the same field
					$nextLineFrom = "yes";
				} else {																			# this is the last (or only) line of the field
					$nextLineFrom = "";
				}
			} elsif ($line =~ m/^Reply-To:\s(.*)$/){
				$replyTo	.= $1;																	# store the "Reply-To:" line
				$replyTo	=~ s/^(.*)[<](.*)[>]$/$2/;
				$fullEmailArray[$i] = "[EMPTY_LINE]";												# delete the line				
			}			
			$i++;
		}

		if ($development eq "yes"){
			$deliveredTo = "frode\@klevstul.com";
		}

		$newFrom = $to;																				# putting together a new From field, starting with $to in case of multiple receivers
		if ($cc ne ""){
			$newFrom .= "," . $cc;																	# ... adding the CC if it's not empty
		}
		$newFrom =~ s/\s{2,}//g;
		$newFrom =~ s/\t//g;
		if (&validEmail($forwardAddress)){
			$newFrom =~ s/$forwardAddress//g;														# remove addresses that we don't wont keep as From addresses
		}
		if (&validEmail($deliveredTo)){
			$newFrom =~ s/$deliveredTo//g;
		}
		if (&validEmail($fromAddress)){
			$newFrom =~ s/$fromAddress//g;
		}
		if (&validEmail($replyTo)){
			$newFrom =~ s/$replyTo//g;
		}
		
		$fromName =~ s/\"//sg;																		# remove any '"' characters
		
		if ($replyTo ne ""){
			$newFrom = "From: \"$fromName (via kAntiSpam)\" <$replyTo>" . "," . $newFrom;			# adding the first part to the From field
		} else {
			$newFrom = "From: \"$fromName (via kAntiSpam)\" <$fromAddress>" . "," . $newFrom;		# adding the first part to the From field		
		}

		$newFrom =~ s/[\d\w\s\-\"]*\<\s*\>//g;														# '<>' --> ''
		$newFrom =~ s/\,\s*\,/,/g;																	# ',,' --> ','
		$newFrom =~ s/\,\s*$//g;																	# remove the last ',' on the line
		
		$fullEmailArray[0] = $newFrom;																# array position '0' is always the previous from (start of email line) so we can overwrite this one

		if ($development eq "yes"){
			open (MAIL, ">&STDOUT");
		} else {
			open (MAIL, "|$sendmailPath -t") || &error("starting sendmail '$sendmailPath': $!");
		}
		$i = 0;
		foreach $line (@fullEmailArray){
			if ($line !~ /^\[EMPTY_LINE\]$/){
				print MAIL $line . "\n";
			} 
			$i++;
		}
		close(MAIL) || &error("closing mail: $!");

		&debug("Mail successfully forwarded", 2);
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# goes through a bounced email's body and returns the address that bounced
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub getBouncedEmailaddress{
	my $body = $_[0];

	my $validEmailAddress	= '[a-zA-Z0-9]+[a-zA-Z0-9_\-\.]*\@[a-zA-Z0-9_\-\.]*\.[a-zA-Z]{2,4}';		# regExp for a valid email address

	my $errorLine1 = '----- The following addresses had permanent \w{0,10} errors -----';			# hotmail/aol uses this error
	my $errorLine2 = 'Sorry it didn\'t work out.';													# qmail mailserver uses this
	my $errorLine3 = 'The SMTP Server program';														# SMTP mailserver
	my $errorLine4 = 'Delivery to the following recipients failed.';								# Delivery Status Notification
	my $errorLine5 = 'Your message[\n\s]+To:\s+';													# unknown
	my $bouncedEmail;
	
	# the bounced email address is always (?) the first email address in the body of the email
	if ($body =~ m/($validEmailAddress)/s){
		$bouncedEmail = $1;
		return $bouncedEmail;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# clean up emails from bounced email addresses
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub cleanUp{
	my $mailFile = $_[0];
	my $i;
	my $mailContent;

	if ($mailFile !~ m/DELETE_IMMEDIATELY/){														# ...in case the deleted emails aren't stored
		&parseMailfolder($mailFile);

		FOR: for ($i=1; $i<=$emailCounter; $i++){
			if ( $bouncedEmailAddresses =~ m/$from{$i}/i ){											# The email is from an address that bounced
				&debug("Email from '$from{$i}' w/subject '$subject{$i}' deleted from '$mailFile' (email address not valid/bounced) ",2);
				next FOR;																			# so we won't store this address
			} else {
				$mailContent .= $fullEmail{$i};
			}
		}
	
		open (FILE, ">$mailFile") || &error("Could not open '$mailFile'");							# overwrite old data with new clean content
		flock (FILE, 2);																			# lock file for writing
		print FILE $mailContent;
		close (FILE);
		flock (FILE, 8);																			# unlock file
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# update (save) notification logfile
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub updateNotificationLogfile{
	open (FILE, ">$notifiedFile") || &error("Could not open '$notifiedFile'");
	flock (FILE, 2);
	foreach (sort keys %notificationLogEntries){
		print FILE $_ . "\t" . $notificationLogEntries{$_} . "\n";
	}
	close (FILE);
	flock (FILE, 8);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# write lockfile to prevent multiple programs running
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub lock{

	if (-e $lockFile){
		&error("kAntiSpam terminated abnormally (lockfile still exsists). Delete '$lockFile' and try again. Contact developer if the problem persists!");
	} else {
		open (FILE, ">$lockFile") || &error("Could not open '$lockFile'");
		flock (FILE, 2);
		print FILE "";
		close (FILE);
		flock (FILE, 8);
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# delete lockfile
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub unlock{

	if (-e $lockFile){
		unlink $lockFile;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# validate email address
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub validEmail{
	my $emailAddress		= $_[0];
	my $validEmailAddress	= '^[a-zA-Z0-9]+.+\@.+\.[a-zA-Z]{2,4}$';								# the regular expression identifying a valid email address

	if( valid($emailAddress) ){																		# check in Address.pm
		if ($emailAddress =~ m/$validEmailAddress/){												# plus own check
			return 1;
		}
	}
	return 0;
}
