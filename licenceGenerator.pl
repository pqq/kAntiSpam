#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename:	licenceGenarator.pl
# author:	Frode Klevstul (frode@klevstul.com) (www.klevstul.com)
# started:	21.03.2004
# ------------------------------------------------------------------------------------


# -------------------
# use
# -------------------
use strict;


# -------------------
# declarations
# -------------------
my $email = "frode\@klevstul.com";
my $licence = "928172-837612-179425-100952";
my $magic = "180876";

my $emailValue = &getEmailValue($email);


my ($n1,$n2,$n3,$n4) = split('-',$licence);
my $i;
my $result;

# ***********************
# valid if:
#
# $magic == $n1 - $n2 + $n3 - $n4 + $emailValue;
# ***********************



# -------------------
# main
# -------------------
$i = $magic + $n2 - $n3 + $n4 - $emailValue;
print "\$n1 = " . $i . "\n";

$i = - $magic + $n1 + $n3 - $n4 + $emailValue;
print "\$n2 = " . $i . "\n";

$i = $magic - $n1 + $n2 + $n4 - $emailValue;
print "\$n3 = " . $i . "\n";

$i = - $magic + $n1 - $n2 + $n3 +$emailValue;
print "\$n4 = " . $i . "\n";

print "\$eV = $emailValue [$email]\n";

$result = $n1 - $n2 + $n3 - $n4 + $emailValue;

print "\nresult: $result\n\n";

if ($result == $magic){
	print "VALID LICENCE!\n";
} else {
	print "INVALID LICENCE!\n";
}





sub getEmailValue{
	my $email = $_[0];

	# ---------------------------------
	# example:
	#
	# email: frode@klevstul.com
	# part1: frode
	# part2: klevstul.com
	# part3: frode@klevstul
	# part4: com
	# ---------------------------------

	my ($part1,$part2) = split('\@',$email);
	my ($part3,$part4) = split('\.',$email);

	my $sum = ((length($part1) * length($part3) + length($part2) * length($part3)) * length($part4)) * length($part3);

	my $sum2 = 0;
	my @emailCharacter = split(//,$email);

	foreach (@emailCharacter){
		$sum2 += ord($_);
	}

	my $totalSum = $sum + $sum2;

	return $totalSum;
}
