# 1 |  ----------------------------------------------------------------------------	|
# 2 |  the following should be escaped (with '\' if you are trying to match that 	|
# 3 |  character (because of regular expression):									|
# 4 |  \  ^  .  $  |  (  )  [  ]													|
# 5 |  *  +  ?  {  }  ,																|
# 6 |  ----------------------------------------------------------------------------	|
